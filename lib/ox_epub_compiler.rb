# Copyright 2012-02-22, Massimo Maria Ghisalberti, Pragmas snc
# http://pragmas.org. All Rights Reserved.
# This is free software.
# licence: BSD 2-clause license ("Simplified BSD License" or "FreeBSD License")
# <http://www.freebsd.org/copyright/copyright.html>

# Ideas and basic implementations
# Massimo Maria Ghisalberti <massimo.ghisalberti@gmail.com, zairik@gmail.com,
# minimal.procedure@gmail.com >

# Epubcheck: https://code.google.com/p/epubcheck/
# ...

# STATE: BETA --

# encoding: UTF-8

require 'rubygems'
require 'bundler/setup'

gem 'sass'
require 'sass'
require 'fileutils'
require 'uuidtools'
require 'yaml'
require 'optparse'
require 'redcloth'
require 'kramdown'
require 'haml'
require 'erb'
require 'zip'

%w[
  .
  ./lib
].each do |dep|
  $LOAD_PATH.unshift(File.dirname(__FILE__) + "/#{dep}")
end
require 'natcmp'

class OxEpubCompiler
  VERSION = %w[0 6 2].freeze

  COVER_PAGE_NAME = 'index.xhtml'.freeze # 'mainpage.xhtml'
  PAGE_PREFIX = 'page_'.freeze
  IMAGE_PREFIX = 'img_'.freeze
  WRAP_COVER = false

  TEXTILES = %w[txt textile].freeze
  MARKDOWN = %w[md].freeze
  HAMLS = %w[haml].freeze
  ERBS = %w[erb].freeze
  MISCS = [].freeze
  IMAGES = %w[png gif jpg].freeze

  COVER_SYM = '⏻'.freeze
  INDEX_SYM = '⌂'.freeze
  PREV_SYM = '«'.freeze
  NEXT_SYM = '»'.freeze

  def initialize(basepath, generationpath, options = {})
    @timenow = Time.now
    @basepath = File.expand_path(basepath)
    @generationpath = File.expand_path(generationpath)

    # source paths
    @root_source_styles = File.join(@basepath, 'Styles')
    @root_source_texts = File.join(@basepath, 'Texts')
    @root_source_images = File.join(@basepath, 'Images')
    @root_source_fonts = File.join(@basepath, 'Fonts')

    @gen_options = {
      logfile: nil,
      verbose: false,
      preserve: false,
      name: 'epub.root',
      epub: false,
      check: false
    }
    @gen_options.merge!(options)
    @options = YAML.safe_load(File.open(File.join(@basepath, 'metadata.yml')))

    @gen_options.merge!(@options['generation']) if @options['generation']
    @gen_options[:verbose] = true if @gen_options[:logfile]

    # target paths
    @root = File.join(@generationpath, @gen_options[:name])
    @root_meta_inf = File.join(@root, 'META-INF')
    @root_oebps = File.join(@root, 'OEBPS')
    @root_oebps_styles = File.join(@root_oebps, 'Styles')
    @root_oebps_texts = File.join(@root_oebps, 'Texts')
    @root_oebps_images = File.join(@root_oebps, 'Images')
    @root_oebps_fonts = File.join(@root_oebps, 'Fonts')

    @options['uuid'] = UUIDTools::UUID.timestamp_create unless @options['uuid']
    @options['generator'] = self.class.to_s
    @options['generator_version'] = VERSION.join('.')
    @options['version'] = '0.0.0.0' unless @options['version']
    @options['build'] = @timenow.strftime('%Y%m%d%H%M%S')
    @options['DC']['publication'] = @timenow.strftime('%Y-%m-%d')
    @options['DC']['modification'] = @timenow.strftime('%Y-%m-%d')

    File.open(File.join(@basepath, 'metadata.yml'), 'w') do |f|
      f.write(@options.to_yaml)
    end

    #@options['DC']['hasVersion'] = %(#{@options['version']}.#{@options['build']})

    @options['images'] = [] unless @options['images']

    @options['pre_pages'] =
      if @options['pre_pages']
        @options['pre_pages'].map { |doc| File.join(@root_source_texts, doc) }
      else
        []
      end

    @options['post_pages'] =
      if @options['post_pages']
        @options['post_pages'].map { |doc| File.join(@root_source_texts, doc) }
      else
        []
      end

    @options['excludes'] =
      if @options['excludes']
        @options['excludes'].map { |doc| File.join(@root_source_texts, doc) }
      else
        []
      end

    @options['excludes'].push(File.join(@root_source_texts, @options['cover'])) unless cover_is_image?

    @wrapped_files = []

    extensions = (TEXTILES + MARKDOWN + HAMLS + ERBS + MISCS).join(',')
    documents = Dir[@root_source_texts + "/**/*.{#{extensions}}"]
    documents = documents.map do |doc|
      if @options['pre_pages'].include?(doc) ||
         @options['post_pages'].include?(doc) ||
         @options['excludes'].include?(doc) ||
         File.basename(doc).match(/^_/)

        nil
      else
        doc.gsub(@root_source_texts, '')
      end
    end
    documents.compact!
    documents.natural_sort!
    documents = analyze(documents)
    documents = flatten(documents, :root)

    @files = []
    @files.push(@options['pre_pages'])
    @files.push(documents)
    @files.push(@options['post_pages'])
    @files.flatten!

    File.open(File.join(@basepath, 'filelist.yml'), 'w') do |f|
      f.write(@files.to_yaml)
    end
  end

  def compile
    log("#{@options['generator']} version #{@options['generator_version']}")
    log('Initialization...')
    log("source: #{@root_source_texts}")
    log("target: #{@generationpath}")
    unless File.exist?(@generationpath)
      log("Prepare target: #{@generationpath}")
      FileUtils.mkdir_p(@generationpath)
    end
    prepare_structure
    @headings = []
    @current_index = 0
    @files.map! { |file| file if File.exist?(file) }
    @files.compact!
    @files.each do |file|
      log("Processing file: #{file}")
      @current_file = file
      File.open(@current_file, 'r:utf-8') do |code|
        @current_text = code.read
        normalize_unicode!(@current_text)
      end

      @headings.push(prepare_heading)
      prepare_wrapper
      if @headings[@current_index].strip == 'autoindex'
        @first_index ||= @current_index
        @headings[@current_index] = 'index'
        @autoindex_page_files ||= {}
        @autoindex_page_files[@current_index] = @current_text
      end
      @current_index += 1
    end

    # if @autoindex_page_files
    @autoindex_page_files&.each do |k, v|
      @current_index = k.to_i
      @current_text = v
      @autoindex = true
      prepare_wrapper
    end
    # end

    prepare_metas
    epub if @gen_options[:epub]
    log('Done.')
  end

  def normalize_unicode!(text)
    enc = lambda {
      text.encode!('UTF-16', 'UTF-8')
      text.encode!('UTF-8', 'UTF-16')
    }
    enc unless text.encoding.name.upcase == 'UTF-8'
  end

  def epub
    log('Prepare epub package...')
    epub_path = File.join(@generationpath, "#{@gen_options[:name]}.epub")
    File.unlink(epub_path) if File.exist?(epub_path)
    epub_contents = Dir[File.join(@generationpath, @gen_options[:name], '**/*')]

    # mimetype must be the first entry and not compressed
    mime = File.join(@generationpath, @gen_options[:name], 'mimetype')
    Zip::OutputStream.open(epub_path) do |ozip|
      entry = Zip::Entry.new(mime, 'mimetype')
      ozip.put_next_entry(entry, nil, nil, Zip::Entry::STORED)
      ozip.write(IO.read(mime))
    end

    Zip::File.open(epub_path, Zip::File::CREATE) do |zipfile|
      epub_contents.each do |doc|
        parentpath = "#{File.join(@generationpath, @gen_options[:name])}/"
        dir_or_file = doc.gsub(parentpath, '')
        if File.directory?(doc)
          zipfile.mkdir(dir_or_file)
        else
          zipfile.add(dir_or_file, doc) unless doc == mime
        end
      end
    end

    checkfn = lambda {
      path = __dir__
      command = "java -jar #{path}/epubcheck/epubcheck.jar #{epub_path}"
      $stdout.sync = true # That's all it takes...
      IO.popen("#{command} 2>&1") do |pipe| # Redirection is performed using
        # operators
        pipe.sync = true
        while str = pipe.gets
          log(str.strip) # This is synchronous!
        end
      end
    }
    checkfn if @gen_options[:check]
  end

  def relative_paths(path)
    parents = File.dirname(path).split('/')
    if parents.empty? && parents[0] == '.'
      parents = []
    else
      parents.map! { |part| part unless part.empty? }
      parents.compact!
    end
    parents
  end

  def relativize_path(path)
    parents = relative_paths(path)
    parents.map! { |_part| '..' }
    parents.join('/')
  end

  ###
  def include_hash?(sub, struct)
    h = find_hash(sub, struct)
    h && h.keys[0] == sub ? true : false
  end

  def find_hash(sub, struct)
    h = struct.take_while { |e| e.is_a?(Hash) && e.keys[0] == sub ? false : true }
    h.size < struct.size && struct[h.size].is_a?(Hash) ? struct[h.size] : nil
  end

  def flatten(htree, parent = :root, parents = nil, accu = nil)
    accu ||= []
    parents = (parent == :root ? [''] : parents << parent.to_s)
    htree.each do |list|
      rooteds = (list[1].map { |doc| doc if doc.is_a?(Array) }).compact
      subs = (list[1].map { |doc| doc if doc.is_a?(Hash) }).compact
      parent_path = parents.join('/')
      rooted = []
      rooteds.each do |doc|
        rooted << File.join(@root_source_texts, parent_path, doc[0].to_s)
      end
      rooted.natural_sort!
      accu.concat(rooted)
      subs.each_index do |idx|
        flatten(subs[idx], subs[idx].keys[0], parents, accu)
        parents.slice!(-1..-1) if idx < subs.size
      end
    end
    accu
  end

  def sort_tree!(htree)
    htree = htree[htree.keys[0]] if htree.is_a?(Hash)
    htree.sort! do |a, b|
      if a.is_a?(Array) && b.is_a?(Array)
        0
      elsif a.is_a?(Hash) && b.is_a?(Array)
        1
      elsif a.is_a?(Array) && b.is_a?(Hash)
        -1
      end
    end
  end

  def analyze(list, options = {})
    options = {
      index: :store,
      dump: true,
      sort: false
    }.merge(options)
    tree = {}
    tree[:root] = []
    list.each_index do |idx|
      el = list[idx]
      parts = (el.split(%r{/|\\|:}).map { |p| p unless p.empty? }).compact
      if parts.size < 2
        tree[:root] << if options[:index] == :store
                         [parts[0], idx]
                       else
                         parts[0]
                       end
      else
        htree = tree[:root]
        parts.each do |part|
          if parts.last == part
            htree << if options[:index] == :store
                       [part, idx]
                     else
                       part
                     end
          else
            htree << { part => [] } unless include_hash?(part, htree)
            htree = find_hash(part, htree)[part]
          end
          sort_tree!(htree) if options[:sort]
        end
      end
    end
    if options[:dump]
      index = File.join(@basepath, 'index.yml')
      File.open(File.join(@basepath, 'index.yml'), 'w') { |f| f.write(tree.to_yaml) } unless File.exist?(index)
    end
    sort_tree!(tree) if options[:sort]
    tree
  end

  ###
  def prepare_autoindex
    documents = @files.map { |f| f.gsub(@root_source_texts, '') }
    documents = analyze(documents)

    def render_autoindex(htree, title = :root)
      html = ''
      html << %(<dt class="index-sub-title">#{title}</dt>\n)
      htree.each do |doc|
        if doc.is_a?(Array)
          index = doc[1]
          href = File.join(File.dirname(@files[index].gsub(@root_source_texts, '')), "#{PAGE_PREFIX}#{index}.xhtml")
          href = href[1..-1]
          block = %(<dd>
          <div>
          <a class="index-page-link" href="#{href}">#{@headings[index]}</a>
          <a class="index-page-number" href="#{href}">#{index + 1}</a>
          <div style="clear:both;display:none;">&nbsp;</div>
          </div>
          </dd>\n)
        elsif doc.is_a?(String)
          block = %(<dd>#{doc}</dd>\n)
        elsif doc.is_a?(Hash)
          key = doc.keys[0]
          block = %(
          <dd>
            <dl class="autoindex">
              #{render_autoindex(doc[key], key)}
            </dl>
          </dd>)

        end
        html << block
      end
      html
    end

    %(
    <dl class="autoindex">
      #{render_autoindex(documents[:root], @options['title'])}
    </dl>
    )
  end

  def prepare_manifests
    log('Prepare manifests')
    result = {}
    result[:items] = ''
    result[:itemsref] = ''
    result[:navs] = ''
    # @headings.each_index {|index|
    @files.each_index do |index|
      parent_path = File.dirname(@files[index].gsub(@root_source_texts, ''))
      href = File.join('Texts', parent_path, "#{PAGE_PREFIX}#{index}.xhtml")
      result[:items] << %(<item href="#{href}" id="#{PAGE_PREFIX}#{index}.xhtml" media-type="application/xhtml+xml" />\n)
      result[:itemsref] << %(<itemref idref="#{PAGE_PREFIX}#{index}.xhtml" />\n)
      result[:navs] << %(
        <navPoint id="navPoint-#{index}" playOrder="#{index + 1}">
        <navLabel><text>#{@headings[index]}</text></navLabel>
        <content src="#{href}" />
        </navPoint>\n
      )
    end
    result
  end

  def prepare_dc_metas(modus = :xml)
    log('Prepare Dublin Core metatadata')
    metas = ''
    case modus
    when :xml
      @options['DC'].each do |k, v|
        key = k.to_s
        value = v.to_s
        metas << case key
                 when 'creator'
                   %(<dc:#{key} opf:role="aut">#{value}</dc:#{key}>\n)
                 when 'ISBN', 'ISSN', 'DOI', 'AMAZON', 'GOOGLE'
                   %(<dc:identifier opf:scheme="#{key}">#{value}</dc:identifier>\n)
                 when 'creation', 'publication', 'modification'
                   %(<dc:date opf:event="#{key}">#{value}</dc:date>\n)
                 when 'editor'
                   %(<dc:contributor opf:role="edt">#{value}</dc:contributor>\n)
                 else
                   %(<dc:#{key}>#{value}</dc:#{key}>\n)
                 end
      end
    when :html
      @options['DC'].each do |k, v|
        metas << %(<meta name="dc:#{k}" content="#{v}" />\n)
      end
    end
    metas
  end

  def prepare_images
    image_items = ''
    @options['images'].each_index do |index|
      image_file_name = @options['images'][index]
      ext = File.extname(image_file_name)[1..-1]
      ext = 'jpeg' if ext == 'jpg'
      cp_image(image_file_name)
      image_items << %(<item href="Images/#{image_file_name}" id="#{IMAGE_PREFIX}#{index}" media-type="image/#{ext}" />\n)
    end
    image_items
  end

  def cover_ext
    @cover_ext ||= File.extname(@options['cover'])[1..-1]
    @cover_ext
  end

  def cover_name
    @options['cover']
  end

  def cover_is_image?
    @cover_is_image ||= IMAGES.include?(cover_ext)
    @cover_is_image
  end

  def cp_image(image)
    target = File.join(@root_oebps_images, image)
    source = File.join(@root_source_images, image)
    FileUtils.mkdir_p(File.dirname(target))
    FileUtils.cp(source, target) if File.exist?(source)
  end

  def prepare_cover
    result = {}
    if @options['cover']
      log('Prepare Cover')
      ext = cover_ext

      if cover_is_image?
        cp_image(cover_name)
      else
        @current_index = :cover
        @current_file = File.join(@root_source_texts, @options['cover'])
        File.open(@current_file, 'r') do |code|
          source = code.read.encode('UTF-16', 'UTF-8', invalid: :replace, replace: '')
          @current_text = source.encode('UTF-8', 'UTF-16', invalid: :replace)
        end
        @headings.insert(0, prepare_heading)
        cover_page = prepare_wrapper
        @headings = []
        @current_file = nil
        @current_index = nil
      end

      # normalize for mimetype
      ext = 'jpeg' if ext == 'jpg'
      result[:itemsref] = %(<itemref idref="#{COVER_PAGE_NAME}" />\n)
      result[:navs] = %(
          <navPoint id="navPoint-cover" playOrder="0">
          <navLabel><text>#{COVER_SYM}</text></navLabel>
          <content src="#{COVER_PAGE_NAME}" />
          </navPoint>\n
      )

      image_item = cover_is_image? ? %(<item href="Images/#{cover_name}" id="cover" media-type="image/#{ext}" />\n) : ''
      result[:items] = %(
      #{image_item}
      <item href="#{COVER_PAGE_NAME}" id="#{COVER_PAGE_NAME}" media-type="application/xhtml+xml" />\n
      )
      result[:meta] = %(<meta name="cover" content="cover" />)

      if cover_is_image?
        cover_src =
          if WRAP_COVER
            %(
              <svg
                xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                version="1.1"
                width="100%"
                height="100%"
                viewBox="0 0 531 751"
                preserveAspectRatio="none">
              <image width="531" height="751" xlink:href="../Images/#{cover_name}" />
              </svg>
            )
          else
            %(<img class="cover-image" src="Images/#{cover_name}" alt="cover-image" />)
          end
        first_page_name = "#{PAGE_PREFIX}0.xhtml"

        cover_page = %(
        <?xml version='1.0' encoding='utf-8'?>
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <meta name="#{self.class}:cover" content="true" />
                <title>#{COVER_SYM}</title>
                <meta http-equiv="content-type" content="text/xhtml; charset=utf-8" />
                <meta name="dc:identifier" content="urn:uuid:#{UUIDTools::UUID.timestamp_create}" />
                <meta name="dc:title" content="Cover" />
                #{prepare_dc_metas(:html)}
                <meta content="#{@options['generator_version']}" name="#{@options['generator']}" />
                <style type="text/css" title="override_css">
                    @page {padding: 0pt; margin:0pt}
                    body { text-align: center; padding:0pt; margin: 0pt; }
                    a.cover { padding:0pt; margin:0pt;}
                    img.cover-image { padding:0pt; margin:0pt; height:100%; width:100%; }
                </style>
            </head>
            <body>
                <div>
                  <a class="cover" href="Texts/#{first_page_name}">
                    #{cover_src}
                  </a>
                </div>
            </body>
        </html>
        )
      end

      File.open(File.join(File.dirname(@root_oebps_texts), COVER_PAGE_NAME), 'w') do |f|
        f.puts(cover_page.strip)
      end
      result[:reference] = %(
      <guide>
        <reference href="#{COVER_PAGE_NAME}" type="cover" title="Cover"/>
      </guide>
      )
    end
    result
  end

  def prepare_fonts
    result = {}
    result[:fonts] = ''
    result[:css_fonts] = ''
    return unless @options['fonts']

    log('Prepare fonts')
    # fonts = ''
    if @options['fonts']['normal']
      font = File.basename(@options['fonts']['normal'])
      FileUtils.cp(File.join(@root_source_fonts, @options['fonts']['normal']), File.join(@root_oebps_fonts, font))
      result[:fonts] << %(<item href="Fonts/#{font}" id="#{font}" media-type="application/x-font-ttf" />\n)
      # "#{font.gsub(File.extname(font), '')}";
      result[:css_fonts] << %(
          @font-face {
          font-family: "MainFont-rg";
          font-style: normal;
          font-weight: normal;
          src: url("../Fonts/#{font}");
          }
        )
    end

    if @options['fonts']['bold']
      font = File.basename(@options['fonts']['bold'])
      FileUtils.cp(File.join(@root_source_fonts, @options['fonts']['bold']), File.join(@root_oebps_fonts, font))
      result[:fonts] << %(<item href="Fonts/#{font}" id="#{font}" media-type="application/x-font-ttf" />\n)
      # "#{font.gsub(File.extname(font), '')}";
      result[:css_fonts] << %(
          @font-face {
          font-family: "MainFont-bd";
          font-style: normal;
          font-weight: bold;
          src: url("../Fonts/#{font}");
          }
        )
    end

    if @options['fonts']['italic']
      font = File.basename(@options['fonts']['italic'])
      FileUtils.cp(File.join(@root_source_fonts, @options['fonts']['italic']), File.join(@root_oebps_fonts, font))
      result[:fonts] << %(<item href="Fonts/#{font}" id="#{font}" media-type="application/x-font-ttf" />\n)
      # "#{font.gsub(File.extname(font), '')}";
      result[:css_fonts] << %(
          @font-face {
          font-family: "MainFont-it";
          font-style: normal;
          font-weight: italic;
          src: url("../Fonts/#{font}");
          }
        )
    end

    if @options['fonts']['mono']
      font = File.basename(@options['fonts']['mono'])
      FileUtils.cp(File.join(@root_source_fonts, @options['fonts']['mono']), File.join(@root_oebps_fonts, font))
      result[:fonts] << %(<item href="Fonts/#{font}" id="#{font}" media-type="application/x-font-ttf" />\n)
      # "#{font.gsub(File.extname(font), '')}";
      result[:css_fonts] << %(
          @font-face {
          font-family: "MainFont-mono";
          font-style: normal;
          font-weight: monospace;
          src: url("../Fonts/#{font}");
          }
        )
    end
    result[:css_fonts].strip!
    result[:css_fonts].squeeze!(' ')
    result[:fonts].strip!
    result
  end

  def prepare_structure
    log('Preparing structure...')

    unless @gen_options[:preserve]
      log('Removing old structure...')
      FileUtils.remove_dir(@root, true)
    end

    log("Create #{@root_meta_inf}")
    FileUtils.mkdir_p(@root_meta_inf)

    log("Create #{@root_oebps}")
    FileUtils.mkdir_p(@root_oebps)

    log("Create #{@root_oebps_styles}")
    FileUtils.mkdir_p(@root_oebps_styles)

    log("Create #{@root_oebps_texts}")
    FileUtils.mkdir_p(@root_oebps_texts)

    log("Create #{@root_oebps_images}")
    FileUtils.mkdir_p(@root_oebps_images)

    log("Create #{@root_oebps_fonts}")
    FileUtils.mkdir_p(@root_oebps_fonts)
  end

  def prepare_metas
    log('Prepare metadata...')

    log('Prepare mimetype')
    epubfile = File.join(@root, 'mimetype')
    unless File.exist?(epubfile)
      text = %(application/epub+zip)
      File.open(epubfile, 'w') { |f| f.write(text.strip) }
    end

    log('Prepare container.xml')
    epubfile = File.join(@root_meta_inf, 'container.xml')
    unless File.exist?(epubfile)
      text = %(
      <?xml version="1.0" encoding="UTF-8"?>
      <container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
          <rootfiles>
              <rootfile full-path="OEBPS/content.opf" media-type="application/oebps-package+xml"/>
         </rootfiles>
      </container>
      )
      File.open(epubfile, 'w') { |f| f.write(text.squeeze(' ').strip) }
    end

    manifest = prepare_manifests
    cover = prepare_cover
    fonts = prepare_fonts

    log('Prepare content.opf')
    epubfile = File.join(@root_oebps, 'content.opf')
    unless File.exist?(epubfile)
      text = %(
        <?xml version="1.0" encoding="utf-8" standalone="yes"?>
        <package xmlns="http://www.idpf.org/2007/opf" unique-identifier="BookId" version="2.0">
          <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
            <dc:identifier id="BookId" opf:scheme="UUID">urn:uuid:#{@options['uuid']}</dc:identifier>
            <dc:title>#{@options['title']} - #{@options['build']}</dc:title>
            #{prepare_dc_metas}
            #{cover[:meta]}
            <meta content="#{@options['generator_version']}" name="#{@options['generator']}" />
          </metadata>
          <manifest>
            <item href="toc.ncx" id="ncx" media-type="application/x-dtbncx+xml" />
            <item href="Styles/styles.css" id="styles.css" media-type="text/css" />
            #{cover[:items]}
            #{fonts[:fonts]}
            #{prepare_images}
            #{manifest[:items]}
          </manifest>
          <spine toc="ncx">
            #{cover[:itemsref]}
            #{manifest[:itemsref]}
          </spine>
            #{cover[:reference]}
        </package>
      )
      File.open(epubfile, 'w') { |f| f.write(text.squeeze(' ').strip) }
    end

    log('Prepare toc.ncx')
    epubfile = File.join(@root_oebps, 'toc.ncx')
    unless File.exist?(epubfile)
      text = %(
          <?xml version="1.0" encoding="utf-8"?>
          <!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN"
           "http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">
          <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
            <head>
              <meta name="dtb:uid" content="urn:uuid:#{@options['uuid']}" />
              <meta name="dtb:depth" content="1" />
              <meta name="dtb:totalPageCount" content="#{@headings.size}" />
              <meta name="dtb:maxPageNumber" content="#{@headings.size}" />
            </head>
            <docTitle>
              <text>#{@options['title']}</text>
            </docTitle>
            <navMap>
              #{cover[:navs]}
              #{manifest[:navs]}
            </navMap>
          </ncx>
      )
      File.open(epubfile, 'w') { |f| f.write(text.squeeze(' ').strip) }
    end

    log('Prepare styles.css')
    epubfile = File.join(@root_oebps_styles, 'styles.css')
    styles_path = File.join(@root_source_styles, @options['css'])
    if @options['css'] && File.exist?(styles_path)
      File.open(epubfile, 'w') do |f|
        f.write("@charset 'utf-8';\n\n")
        f.write("#{fonts[:css_fonts]}\n\n")
        File.open(styles_path, 'r') do |css|
          engine = Sass::Engine.new(css.read, syntax: :scss)
          # engine.render
          f.write(engine.render)
        end
      end
    else
      unless File.exist?(epubfile)
        text = %(/* Book Styles */\n #{fonts[:css_font]})
        File.open(epubfile, 'w') do |f|
          f.write(text.squeeze(' ').strip)
        end
      end
    end
  end

  # def prepare_heading2
  #  log('Prepare heading')
  #  regexp = /\[title:\s*(.*?)\s*\]/im
  #  tokens = @current_text.match(regexp)
  #  if tokens
  #    @current_text.gsub!(regexp, '')
  #    @current_text.strip!
  #    tokens[1]
  #  else
  #    name = File.basename(@current_file).gsub(File.extname(@current_file), '').downcase.split('-')
  #    name[0].gsub(/[_|-]/, ' ').gsub(' l ', "l'").gsub(' c ', "c'")
  #  end
  # end

  def prepare_heading
    log('Prepare heading')

    title_by_match = lambda { |regexp, yaml|
      tokens = @current_text.match(regexp)
      if tokens
        @current_text.gsub!(regexp, '')
        @current_text.strip!
        yaml ? YAML.safe_load(tokens[1])['title'] : tokens[1]
      else
        ''
      end
    }

    title_by_file = lambda {
      name = File.basename(@current_file).gsub(File.extname(@current_file), '').downcase.split('-')
      name[0].gsub(/[_|-]/, ' ').gsub(' l ', "l'").gsub(' c ', "c'")
    }

    t = title_by_match.call(/\[title:\s*(.*?)\s*\]/im, false)
    t = t.empty? ? title_by_match.call(/---\n(.*?)\n---/im, true) : t
    t.empty? ? title_by_file.call : t
  end

  def macro!(text, macro = nil, _subst = nil)
    regexp = if macro
               /\[(#{macro}):\s*(.*?)\s*\]/im
             else
               /\[(\w*):\s*(.*?)\s*\]/im
             end
    tokens = text.scan(regexp) if text
    return if tokens.empty?

    tokens.each do |key, value|
      regexp = /\[#{key}:\s*#{value}\s*\]/im
      case key.to_sym
      when :resource
        case value.to_sym
        when :autoindex
          text.gsub!(regexp, prepare_autoindex)
        when :title
          text.gsub!(regexp, @headings[@current_index])
        when :index
          text.gsub!(regexp, @current_index.to_s)
        when :date
          text.gsub!(regexp, @timenow.strftime('%Y-%m-%d'))
        when :time
          text.gsub!(regexp, @timenow.strftime('%H:%M:%S'))
        end
      when :metadata
        # textile problem
        value.gsub!(%(<span class="caps">), '')
        value.gsub!(%(</span>), '')
        #--
        metas = value.strip.split('.')
        value = ''
        if metas.size == 1
          value = @options[metas[0]]
        else
          metas.each do |meta|
            value = if meta == metas.first
                      @options[meta]
                    else
                      value[meta]
                    end
          end
        end

        if value.nil?
          value = ''
        elsif value.is_a?(Array)
          block = %(<ul class="metadata-list">)
          value.each do |v|
            block << %(<li class="metadata-list-item">#{v}</li>)
          end
          value = "#{block}</ul>"
        elsif value.is_a?(Hash)
          block = %(<ul class="metadata-list">)
          value.each do |k, v|
            block << %(<li class="metadata-list-item">
                  <span class="metadata-list-item-key">#{k}</span>
                  <span class="metadata-list-item-value">#{v}</span>
                  </li>)
          end
          value = "#{block}</ul>"
        end
        text.gsub!(regexp, value.to_s)
      when :partial
        # textile problem
        value.gsub!(%(<span class="caps">), '')
        value.gsub!(%(</span>), '')
        #--
        source = ''
        partial = File.join(@root_source_texts, value)
        if File.exist?(partial)
          File.open(partial, 'r') { |f| source << f.read }
          extname = File.extname(partial)[1..-1]
          if TEXTILES.include?(extname)
            source = RedCloth.new(source.strip).to_html
            source.gsub!(%r{^<p>|</p>$}, '')
          elsif MARKDOWN.include?(extname)
            source = Kramdown::Document.new(source.strip).to_html
            source.gsub!(%r{^<p>|</p>$}, '')
          elsif HAMLS.include?(extname)
            source = Haml::Engine.new(source.strip).render
          elsif ERBS.inlcude?(extname)
            source = ERB.new(source.strip).result
          else
            source = text.strip
          end
        end
        text.gsub!(regexp, source)
      end
    end
  end

  def prepare_wrapper_relative_path(current_relative_path, index)
    fname = File.join(current_relative_path, File.dirname(@files[index].gsub(@root_source_texts, '')),
                      "#{PAGE_PREFIX}#{index}.xhtml")
    if fname[0] == '/'
      fname[1..-1]
    else
      fname
    end
  end

  def prepare_wrapper
    log('wrapping text')

    is_cover = false
    is_cover = true if @current_index.is_a?(Symbol) && @current_index == :cover
    current_relative_path = relativize_path(@current_file.gsub(@root_source_texts, ''))

    if is_cover
      prev_page = ''
      next_page = %(<a class="next" href="#{prepare_wrapper_relative_path(current_relative_path, 0)}">next</a>)
    else
      prev_page = @current_index - 1

      index_link = if @first_index
                     %(<a class="prev index-anchor"
                        href="#{prepare_wrapper_relative_path(current_relative_path, @first_index)}">
                        #{INDEX_SYM}
                        </a>)
                   else
                     %(<a class="prev cover-anchor"
                        href="../#{COVER_PAGE_NAME}">#{COVER_SYM}</a>)
                   end

      prev_page = if prev_page >= 0
                    %(#{index_link}<a class="prev"
                      href="#{prepare_wrapper_relative_path(current_relative_path, prev_page)}">
                      #{PREV_SYM}
                      </a>)
                  else
                    %(<a class="prev cover-anchor" href="../#{COVER_PAGE_NAME}">#{COVER_SYM}</a>)
                  end

      next_page = @current_index + 1
      next_page = if next_page <= @files.size - 1
                    %(<a class="next"
                      href="#{prepare_wrapper_relative_path(current_relative_path, next_page)}">
                      #{NEXT_SYM}
                      </a>)
                  else
                    %(<a class="next cover-anchor" href="../#{COVER_PAGE_NAME}">#{COVER_SYM}</a>)
                  end
    end

    @current_text.strip!

    source = ''
    extname = File.extname(@current_file)[1..-1]
    source = if TEXTILES.include?(extname)
               RedCloth.new("#{@current_text}  ").to_html
             elsif MARKDOWN.include?(extname)
               Kramdown::Document.new("#{@current_text}  ").to_html
             elsif HAMLS.include?(extname)
               Haml::Engine.new(@current_text).render
             elsif ERBS.include?(extname)
               ERB.new(@current_text).result
             else
               @current_text.strip
             end

    normalize_unicode!(source)

    macro!(source)

    @autoindex = nil if @autoindex

    if is_cover
      @current_index = 0
      style_parent_path = ''
    else
      style_parent_path = File.join('..', current_relative_path, '/')
    end

    page_head = if is_cover
                  %(
                    <div class="content-navigation-next">
                    #{next_page}
                    <span style="clear:both;display:none;">&nbsp;</span>
                    </div>
                    )
                else
                  %(
                    <div class="content-head">
                    <h1 class="title">
                    #{prev_page}
                    <span class="caption">#{@headings[@current_index]}</span>
                    #{next_page}
                    <span style="clear:both;display:none;">&nbsp;</span>
                    </h1>
                    </div>
                    )
                end

    html = %(
    <?xml version="1.0" encoding="utf-8"?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title>#{@headings[@current_index]}</title>
        <link rel="schema.dc" href="http://purl.org/dc/elements/1.1/" />
        <meta http-equiv="content-type" content="text/xhtml; charset=utf-8"/>
        <meta name="dc:identifier" content="urn:uuid:#{UUIDTools::UUID.timestamp_create}"  />
        <meta name="dc:title" content=#{@headings[@current_index].dump} />
        #{prepare_dc_metas(:html)}
        <meta content="#{@options['generator_version']}" name="#{@options['generator']}" />
        <link charset="utf-8" href="#{style_parent_path}Styles/styles.css" rel="stylesheet" type="text/css" />
      </head>
      <body>
        <div id="content" class="content">
        #{!is_cover ? page_head : ''}
        #{source}
        #{is_cover ? page_head : ''}
        </div>
      </body>
    </html>)
    parent_path = File.dirname(@current_file.gsub(@root_source_texts, ''))
    target_path = File.join(@root_oebps_texts, parent_path)
    FileUtils.mkdir_p(target_path)
    normalize_unicode!(html)
    unless is_cover
      File.open(File.join(target_path, "#{PAGE_PREFIX}#{@current_index}.xhtml"), 'w:utf-8') do |f|
        f.write(html.strip)
      end
    end
    html.strip
  end

  def log(message)
    message = message.to_s
    if @gen_options[:logfile]
      File.open(@gen_options[:logfile], 'a') do |log|
        log.write(%(LOG: #{message}\n))
      end
    end
    p %(LOG: #{message}) if !@gen_options[:logfile] && (@gen_options[:verbose])
  end

  private :epub, :prepare_heading, :prepare_images, :prepare_wrapper, :macro!
  private :prepare_structure, :prepare_metas, :prepare_manifests, :relative_paths
  private :prepare_dc_metas, :prepare_cover, :prepare_fonts, :log, :relativize_path
  private :prepare_autoindex, :cover_is_image?, :cover_ext, :cover_name, :cp_image
  private :analyze, :find_hash, :include_hash?, :sort_tree!, :flatten, :normalize_unicode!
end

if __FILE__ == $0

  builder_options = {}

  optparse = OptionParser.new do |opts|
    opts.banner = 'Usage: epub-compiler [options] folderProject folderGeneration'

    # builder_options[:verbose] = false
    opts.on('-v', '--verbose', 'Output more information') do
      builder_options[:verbose] = true
    end

    # builder_options[:logfile] = nil
    opts.on('-l', '--logfile FILE', 'Write log to FILE') do |file|
      builder_options[:logfile] = file
    end

    # builder_options[:preserve] = false
    opts.on('-p', '--preserve', "Don't remove old generation") do
      builder_options[:preserve] = true
    end

    # builder_options[:name] = 'epub.root'
    opts.on('-n', '--name Name', 'Folder name of generation') do |name|
      builder_options[:name] = name
    end

    # builder_options[:epub] = false
    opts.on('-e', '--epub', 'Generate epub') do |_name|
      builder_options[:epub] = true
    end

    # builder_options[:check] = false
    opts.on('-c', '--check', 'Check epub validity (require java)') do |_name|
      builder_options[:check] = true
    end

    opts.on('-h', '--help', 'Display this screen') do
      puts opts
      exit
    end
  end

  optparse.parse!

  case ARGV.size
  when 0
    p optparse.banner
  when 1
    gen = OxEpubCompiler.new(ARGV[0], ARGV[0], builder_options)
    gen.compile
  when 2
    gen = OxEpubCompiler.new(ARGV[0], ARGV[1], builder_options)
    gen.compile
  else
    p optparse.banner
  end

end
